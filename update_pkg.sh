#!/bin/bash
set -e
PKGNAME="metadata-backup"
CRATES_API="https://crates.io/api/v1/crates/$PKGNAME/"
VERSIONS_FILE=".VERSIONS"

download_versions() {
    curl $CRATES_API/versions | \
    jq -r '.versions | .[] | .num' | \
    sort -V -r | \
    head -n 1 \
    > $VERSIONS_FILE
}

get_latest_version() {
    # Determine what the latest version is on crates.io
    download_versions
    echo $(head -n 1 $VERSIONS_FILE)
}

assert_version_exists() {
    if [ ! -f $VERSIONS_FILE ]; then
        download_versions
    fi

    if ! grep -x $1 $VERSIONS_FILE; then
        echo "Could not find version $1 of package $PKGNAME."
        echo "If this is a recently released version, try deleting the "
        echo "file $VERSIONS_FILE and trying again."
        exit 1
    fi
}

# User can specify a version with PKGVER=the_version
if [ -z ${PKGVER+x} ]; then
    PKGVER=$(get_latest_version)
else
    assert_version_exists $PKGVER
fi

# Download the tarball if it hasn't already been downloaded
filename=$PKGNAME-$PKGVER.tar.gz
if [ ! -f $filename ]; then
    curl -L $CRATES_API/$PKGVER/download -o $filename
fi

sha512=$(sha512sum $filename | cut -d ' ' -f 1)

# Replace the current values of the relevant variables in the PKGBUILD
echo "Updating PKGBUILD for $PKGVER"
cp PKGBUILD PKGBUILD.bak
cat PKGBUILD.bak | \
    sed '/^pkgver=/c\pkgver='$PKGVER | \
    sed '/^sha512sums=/c\sha512sums=("'$sha512'")' \
    > PKGBUILD
rm PKGBUILD.bak
